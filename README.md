# Sprint3
Objetivo:
El objetivo del presente proyecto es cumplir con todos los requisitos que solicitan para la parobación del sprint 3 (Web backend Acámica)

a- Contar con un dominio propio: sprint3.ml

b- Contar con un certificado de seguridad https: El mismo puede verse reflejado dentro de Certificate manager (AWS). Al ingresar a https://www.sprint3.ml puede verse la respuesta del servidor

c- Antes de pasar al punto c. Se expresa la resolución del punto i:

    Ingresar al siguiente link: https://143159854197.signin.aws.amazon.com/console
    Las credenciales de ingreso del tech reviewer son las siguientes:
    User: tech-reviever1
    Pass: Tech-reviever1

    Ingresar al panel load balancer para verificar información relacionada a este punto

d-  Ingresar al panel EC2, iniciar instancia.
    1- Abrir la consola en donde haya guardado la llave pem
    2- Dirigirse a Conectar la instancia y copiar la línea situada debajo de "ejemplo"
    3- Pegar la línea en consola, luego escribir :yes
    4- escribir el comando cd sprint3/sprintacamica/
    5- Ejecutar el comando pm2 start ecosystem.config.js --env local
    6- Ejecutar el comando sudo nginx

e- Puede verificarse al navegar dentro de AWS

f- El servicio de base de datos es RDS (utilizado en el sprint 2) Las credenciales se encuentran en la ruta src/database/config.js

g- es el presente documento

h- El repositorio con la última versión del código fuente (backend) es la siguiente: https://gitlab.com/LucasLop123/sprint3backend

i- https://www.sprint3.ml/api/api-docs/

j- El health cheack se encuentra configurado cada 60 segundos (se puede verificar en load balancer settings)

k- Las reglas de seguridad pueden verse dentro de "Security groups"

l- Existe una integración continua entre el repositorio (Frontend) y la infraestrucura. Puede observarse dentro de el siguiente link (pipelines) https://gitlab.com/LucasLop123/sprint3

m- El grupo de austoescalado se encuentra creado y funcional, a efectos de evitar gastos extras su configuración fue seteada en 0 0 0 

n- La imagen AMI está creada y puede verse dentro de AWS

o- El bucked S3 y cloudfront están creados y funcionando. Puede verse dentro de el siguiente link: https://d1ab3vuevx4aje.cloudfront.net/

p- El layer de elasticache se encuetra creado y funcionando. Dirigirse a Redis AWS. La configuración en el código puede verse en index. js y src/middlewears/redis.js

q- El mapa de como funciona la infraestructura se adjunta en la entrega


